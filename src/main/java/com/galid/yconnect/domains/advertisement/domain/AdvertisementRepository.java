package com.galid.yconnect.domains.advertisement.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AdvertisementRepository extends JpaRepository<AdvertisementEntity, Long> {

    interface AdvertisementRequestModel {
        Long getAdvertisementId();
        String getAnotherName();
        String getConnectorImageUrl();
        String getAudienceClass();
        String getBroadCastCharacter();
        String getRelatedField();
        String getMatchState();
    }

    @Query(
        value="select advertisement_id as advertisementId, another_name as anotherName, connector_image_url as connectorImageUrl, audience_class as audienceClass, broadcast_character as broadCastCharacter, related_field as relatedField, match_state as matchState " +
                "                from advertisement as a " +
                "                inner join connector as c " +
                "                on a.connector_id = c.connector_id " +
                "                inner join user as u " +
                "                on c.user_id = u.user_id " +
                "where a.advertiser_id = ?1 " +
                "ORDER BY a.advertisement_id DESC",
        nativeQuery = true
    )
    List<AdvertisementRequestModel> findAdvertisementRequestListByCorporationId(long corporationId);

    List<AdvertisementEntity> findByConnectorIdOrderByCreatedDateDesc(long connectorId);

    void removeByAdvertisementId(long advertisementId);
}
