package com.galid.yconnect.domains.advertisement.domain;

public enum MatchState {
    MATCHING("대기중"), ACCEPTED("광고 매칭 성공"), REJECTED("거절"), CANCEL("광고 요청 취소");

    private String state;

    MatchState(String state) {
        this.state = state;
    }

    public String getState() {
        return this.state;
    }
}
