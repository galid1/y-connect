package com.galid.yconnect.domains.advertisement.service;

import com.galid.yconnect.domains.advertisement.domain.AdvertisementEntity;
import com.galid.yconnect.domains.advertisement.domain.AdvertisementRepository;
import com.galid.yconnect.domains.connector.domain.ConnectorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RegisterAdvertisementService {
    @Autowired
    private AdvertisementRepository advertisementRepository;

    @Autowired
    private ConnectorRepository connectorRepository;

    @Transactional
    public void registerAdvertisementRequest(long advertiserId, RegisterAdvertisementRequest request) {
        AdvertisementEntity advertisementEntity = AdvertisementEntity.builder()
                .connectorId(request.getConnectorId())
                .advertiserId(advertiserId)
                .advertiserInformation(request.getAdvertiserInformation())
                .advertisementRequestInformation(request.getAdvertisementRequestInformation())
                .build();

        advertisementRepository.save(advertisementEntity);
    }


}
