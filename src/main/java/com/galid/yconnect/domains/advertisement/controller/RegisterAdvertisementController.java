package com.galid.yconnect.domains.advertisement.controller;

import com.galid.yconnect.domains.advertisement.service.RegisterAdvertisementRequest;
import com.galid.yconnect.domains.advertisement.service.RegisterAdvertisementService;
import com.galid.yconnect.domains.user.domain.UserEntity;
import com.galid.yconnect.domains.user.domain.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegisterAdvertisementController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RegisterAdvertisementService registerAdvertisementService;

    @PostMapping("/advertisements")
    public ResponseEntity registerAdvertisement(Authentication authentication,
                                                @RequestBody RegisterAdvertisementRequest request) {
        UserEntity advertiser = userRepository.findByEmail(authentication.getName()).get();
        registerAdvertisementService.registerAdvertisementRequest(advertiser.getUserId(), request);

        return ResponseEntity.ok().build();
    }
}
