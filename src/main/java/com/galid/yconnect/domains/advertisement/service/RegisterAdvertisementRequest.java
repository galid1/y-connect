package com.galid.yconnect.domains.advertisement.service;

import com.galid.yconnect.domains.advertisement.domain.AdvertisementRequestInformation;
import com.galid.yconnect.domains.advertisement.domain.AdvertiserInformation;
import lombok.*;

@Getter
@AllArgsConstructor
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class RegisterAdvertisementRequest {
    private long connectorId;
    private AdvertiserInformation advertiserInformation;
    private AdvertisementRequestInformation advertisementRequestInformation;
}
