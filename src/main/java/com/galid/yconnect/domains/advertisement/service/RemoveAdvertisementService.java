package com.galid.yconnect.domains.advertisement.service;

import com.galid.yconnect.domains.advertisement.domain.AdvertisementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class RemoveAdvertisementService {
    private final AdvertisementRepository advertisementRepository;

    public void removeAdvertisement(Long advertisementId) {
        advertisementRepository.removeByAdvertisementId(advertisementId);
    }
}
