package com.galid.yconnect.domains.advertisement.domain;

import com.galid.yconnect.common.config.BaseEntity;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "advertisement")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ToString
public class AdvertisementEntity extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long advertisementId;

    private long connectorId;
    private long advertiserId;

    @Embedded
    private AdvertiserInformation advertiserInformation;

    @Embedded
    private AdvertisementRequestInformation advertisementRequestInformation;

    private String matchState;

    @Builder
    public AdvertisementEntity(long connectorId, long advertiserId, AdvertiserInformation advertiserInformation, AdvertisementRequestInformation advertisementRequestInformation) {
        this.connectorId = connectorId;
        this.advertiserId = advertiserId;
        this.advertiserInformation = advertiserInformation;
        this.advertisementRequestInformation = advertisementRequestInformation;
        this.matchState = MatchState.MATCHING.getState();
    }

    public void accept() {
        verifyNotYetDecision();
        this.matchState = MatchState.ACCEPTED.getState();
    }

    public void reject() {
        verifyNotYetDecision();
        this.matchState = MatchState.REJECTED.getState();
    }

    public void cancel() {
        verifyNotYetDecision();
        this.matchState = MatchState.CANCEL.getState();
    }

    private void verifyNotYetDecision() {
        if(isMatched())
            throw new IllegalStateException("이미 처리된 광고입니다.");
    }

    private boolean isMatched() {
        if(!this.matchState.equals(MatchState.MATCHING.getState()))
            return true;
        return false;
    }

}
