package com.galid.yconnect.domains.advertisement.controller;

import com.galid.yconnect.domains.advertisement.service.RemoveAdvertisementService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class RemoveAdvertisementController {
    private final RemoveAdvertisementService removeAdvertisementService;

    @DeleteMapping("/advertisements/{advertisementId}")
    public ResponseEntity getRemoveAdvertisementService(@PathVariable("advertisementId") Long advertisementId) {
        removeAdvertisementService.removeAdvertisement(advertisementId);
        return ResponseEntity.ok().build();
    }
}
