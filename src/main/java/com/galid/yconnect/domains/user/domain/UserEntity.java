package com.galid.yconnect.domains.user.domain;

import com.galid.yconnect.common.config.BaseEntity;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "user")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UserEntity extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long userId;
    private String email;
    private String password;
    private String authority;

    @Embedded
    private UserInformation userInformation;

    @Builder
    public UserEntity(String email, String password, String authority, UserInformation userInformation) {
        this.email = email;
        this.password = password;
        this.authority = authority;
        this.userInformation = userInformation;
    }

    public void modifyInformation(UserInformation userInformation) {
        if(userInformation == null)
            throw new IllegalArgumentException("유저 정보를 입력하세요.");

        this.userInformation = userInformation;
    }
}
