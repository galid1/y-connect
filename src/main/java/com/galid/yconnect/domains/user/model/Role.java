package com.galid.yconnect.domains.user.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
@AllArgsConstructor
@ToString
public class Role {
    private boolean youtuber;
    private boolean corporation;

    public void onYoutuber() {
        this.youtuber = true;
        this.corporation = false;
    }

    public void onCorporation() {
        this.youtuber = false;
        this.corporation = true;
    }
}
