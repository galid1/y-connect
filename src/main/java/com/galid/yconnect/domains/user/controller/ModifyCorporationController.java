package com.galid.yconnect.domains.user.controller;

import com.galid.yconnect.domains.user.service.ModifyCorporationRequest;
import com.galid.yconnect.domains.user.service.ModifyCorporationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class ModifyCorporationController {
    private final ModifyCorporationService corporationService;

    @PutMapping("/users")
    public ResponseEntity modifyCorporation(@RequestBody ModifyCorporationRequest request) {
        corporationService.modifyCorporation(request);

        return ResponseEntity.ok().build();
    }

}
