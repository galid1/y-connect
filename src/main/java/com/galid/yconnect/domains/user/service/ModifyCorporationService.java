package com.galid.yconnect.domains.user.service;

import com.galid.yconnect.domains.user.domain.UserEntity;
import com.galid.yconnect.domains.user.domain.UserInformation;
import com.galid.yconnect.domains.user.domain.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class ModifyCorporationService {
    private final UserRepository userRepository;

    public void modifyCorporation(ModifyCorporationRequest request) {
        UserEntity userEntity = userRepository.findByEmail(request.getEmail())
                .orElseThrow(() -> new IllegalArgumentException("존재하지 않는 기업입니다."));

        userEntity.modifyInformation(toUserInformation(request));
    }

    private UserInformation toUserInformation(ModifyCorporationRequest request) {
        return UserInformation.builder()
                .name(request.getName())
                .anotherName(request.getCorporationName())
                .homePageUrl(request.getHomePageUrl())
                .build();
    }
}
