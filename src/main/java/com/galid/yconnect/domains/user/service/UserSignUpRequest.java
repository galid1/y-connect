package com.galid.yconnect.domains.user.service;

import com.galid.yconnect.domains.user.domain.UserInformation;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UserSignUpRequest {
    private String email;
    private String password;
    private String authority;
    private UserInformation userInformation;
}
