package com.galid.yconnect.domains.user.domain;

import lombok.*;

import javax.persistence.Embeddable;

@Embeddable
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UserInformation {
    private String name;
    private String anotherName;
    private String homePageUrl;
}
