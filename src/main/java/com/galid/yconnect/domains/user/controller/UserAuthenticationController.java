package com.galid.yconnect.domains.user.controller;

import com.galid.yconnect.domains.user.service.UserAuthenticationService;
import com.galid.yconnect.domains.user.service.UserSignUpRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class UserAuthenticationController {
    @Autowired
    private UserAuthenticationService userAuthenticationService;

    @GetMapping("/users/signUp")
    public String getSignUp() {
        return "signUp";
    }

    @PostMapping("/users/signUp")
    @ResponseBody
    public ResponseEntity signUp(@RequestBody UserSignUpRequest request) {
        userAuthenticationService.signUp(request);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/users/signIn")
    public String getSignIn() {
        return "signIn";
    }

    @GetMapping("/policy/{type}")
    public String getPolicy(@PathVariable("type") String type, Model model) {
        if (type.equals("usagePolicy")) {
            model.addAttribute("title", "이용약관");
        }
        else if(type.equals("privacyPolicy")) {
            model.addAttribute("title", "개인정보 처리방침");
        }

        model.addAttribute("type", type);

        return "/policy/policy";
    }

    @PostMapping("/users/checking")
    @ResponseBody
    public ResponseEntity checkDuplicatedEmail(@RequestBody String email) {
        if(userAuthenticationService.checkDuplication(email)){
            return ResponseEntity.badRequest().body("이미 사용중인 이메일입니다!!");
        }
        else {
            return ResponseEntity.ok().body("사용 가능한 이메일입니다.");
        }
    }
}
