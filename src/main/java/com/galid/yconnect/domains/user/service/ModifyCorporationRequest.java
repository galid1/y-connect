package com.galid.yconnect.domains.user.service;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
@Builder
@AllArgsConstructor
public class ModifyCorporationRequest {
    private String email; // 변경할 수 없지만, 유저를 찾는 용도로 사용
    private String name;
    private String corporationName;
    private String homePageUrl;
}
