package com.galid.yconnect.domains.user.service;

import com.galid.yconnect.domains.user.domain.UserEntity;
import com.galid.yconnect.domains.user.domain.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class UserAuthenticationService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Transactional
    public long signUp(UserSignUpRequest request) {
        verifyDuplicated(request);

        UserEntity userEntity = UserEntity.builder()
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .authority(request.getAuthority())
                .userInformation(request.getUserInformation())
                .build();

        return userRepository.save(userEntity).getUserId();
    }

    public boolean checkDuplication(String email) {
        if(userRepository.findByEmail(email).isPresent())
            return true;
        else
            return false;
    }

    private void verifyDuplicated(UserSignUpRequest request) {
        if(userRepository.findByEmail(request.getEmail()).isPresent()) {
            throw new IllegalArgumentException("이미 사용중인 이메일입니다.");
        }
    }
}
