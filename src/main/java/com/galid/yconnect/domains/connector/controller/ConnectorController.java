package com.galid.yconnect.domains.connector.controller;

import com.galid.yconnect.domains.connector.domain.ConnectorEntity;
import com.galid.yconnect.domains.connector.domain.ConnectorRepository;
import com.galid.yconnect.domains.user.domain.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/connectors")
public class ConnectorController {
    @Autowired
    private ConnectorRepository connectorRepository;
    @Autowired
    private UserRepository userRepository;

    @GetMapping("/{connectorId}")
    public String getConnector(@PathVariable("connectorId") long connectorId, Model model) {
        ConnectorEntity connectorEntity = connectorRepository.findById(connectorId).orElseThrow(() -> new IllegalArgumentException("존재하지 않는 커넥터 입니다."));
        String youtubeUrl = userRepository.findById(connectorEntity.getUserId())
                .orElseThrow(() -> new IllegalArgumentException("존재하지 않는 유저입니다."))
                .getUserInformation()
                .getHomePageUrl();

        model.addAttribute("connector", connectorEntity);
        model.addAttribute("youtubeUrl", youtubeUrl);
        return "connectorDetail";
    }
}
