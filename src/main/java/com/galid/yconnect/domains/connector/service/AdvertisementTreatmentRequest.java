package com.galid.yconnect.domains.connector.service;

import lombok.*;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ToString
public class AdvertisementTreatmentRequest {
    private long advertisementId;
    private boolean accept;
}
