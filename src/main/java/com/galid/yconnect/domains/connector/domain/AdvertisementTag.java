package com.galid.yconnect.domains.connector.domain;

import lombok.*;

import javax.persistence.Embeddable;

@Embeddable
@Getter
@AllArgsConstructor
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AdvertisementTag {
    private String relatedField;
    private String audienceClass;
    private String broadcastCharacter;
}
