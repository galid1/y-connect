package com.galid.yconnect.domains.connector.service;

import com.galid.yconnect.domains.advertisement.domain.AdvertisementEntity;
import com.galid.yconnect.domains.advertisement.domain.AdvertisementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ConnectorAdvertisementService {
    @Autowired
    private AdvertisementRepository advertisementRepository;

    @Transactional
    public void treatAdvertisement(AdvertisementTreatmentRequest request) {
        AdvertisementEntity advertisementEntity = advertisementRepository.findById(request.getAdvertisementId())
                .orElseThrow(() -> new IllegalArgumentException("존재하지 않는 광고입니다."));

        if(request.isAccept())
            advertisementEntity.accept();
        else
            advertisementEntity.reject();
    }
}
