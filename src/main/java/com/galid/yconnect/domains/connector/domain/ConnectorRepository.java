package com.galid.yconnect.domains.connector.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ConnectorRepository extends JpaRepository<ConnectorEntity, Long> {
    Optional<ConnectorEntity> findByUserId(long userId);
    List<ConnectorEntity> findAllByOrderByCreatedDateDesc();
}
