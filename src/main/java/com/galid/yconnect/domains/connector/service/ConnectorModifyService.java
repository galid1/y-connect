package com.galid.yconnect.domains.connector.service;

import com.galid.yconnect.common.util.S3FileUtil;
import com.galid.yconnect.domains.connector.domain.AdvertisementTag;
import com.galid.yconnect.domains.connector.domain.ConnectorEntity;
import com.galid.yconnect.domains.connector.domain.ConnectorRepository;
import com.galid.yconnect.domains.connector.domain.WishAdvertisementInformation;
import com.galid.yconnect.domains.user.domain.UserEntity;
import com.galid.yconnect.domains.user.domain.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ConnectorModifyService {
    @Autowired
    private ConnectorRepository connectorRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private S3FileUtil fileUtil;

    public void modifyConnectorInformation(RegisterConnectorRequest request, String userEmail) {
        UserEntity userEntity = userRepository.findByEmail(userEmail)
                .orElseThrow(() -> new IllegalArgumentException("존재하지 않는 유저입니다."));

        ConnectorEntity connectorEntity = connectorRepository.findByUserId(userEntity.getUserId())
                .orElseThrow(() -> new IllegalArgumentException("등록된 유튜버 정보가 없습니다."));

        String connectorImageUrl = null;
        if(request.getConnectorImageFile() != null)
            connectorImageUrl = fileUtil.uploadFile(request.getConnectorImageFile());

        connectorEntity.modifyInformation(toWishAdvertisementInformation(request, connectorImageUrl));
    }

    private WishAdvertisementInformation toWishAdvertisementInformation(RegisterConnectorRequest request, String connectorImageUrl) {
        return WishAdvertisementInformation.builder()
                .advertisementTag(AdvertisementTag.builder()
                        .audienceClass(request.getAdvertisementTag().getAudienceClass())
                        .broadcastCharacter(request.getAdvertisementTag().getBroadcastCharacter())
                        .relatedField(request.getAdvertisementTag().getRelatedField())
                        .build())
                .connectorImageUrl(connectorImageUrl)
                .connectorIntroduce(request.getConnectorIntroduce())
                .pay(request.getPay())
                .period(request.getPeriod())
                .build();
    }
}
