package com.galid.yconnect.domains.connector.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.galid.yconnect.domains.connector.service.RegisterConnectorRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Locale;

@Component
public class RegisterConnectorRequestFormatter implements Formatter<RegisterConnectorRequest> {
    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public RegisterConnectorRequest parse(String text, Locale locale) throws ParseException {
        try {
            return objectMapper.readValue(text, RegisterConnectorRequest.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String print(RegisterConnectorRequest object, Locale locale) {
        return object.toString();
    }
}
