package com.galid.yconnect.domains.connector.domain;

import com.galid.yconnect.common.config.BaseEntity;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "connector")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ConnectorEntity extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long connectorId;

    private long userId;

    @Embedded
    private WishAdvertisementInformation wishAdvertisementInformation;

    @Builder
    public ConnectorEntity(WishAdvertisementInformation wishAdvertisementInformation, long userId) {
        this.wishAdvertisementInformation = wishAdvertisementInformation;
        this.userId = userId;
    }

    public void modifyInformation(WishAdvertisementInformation wishAdvertisementInformationRequest) {
        WishAdvertisementInformation.WishAdvertisementInformationBuilder newInformationBuilder = WishAdvertisementInformation.builder();
        newInformationBuilder.advertisementTag(makeAdvertisementTag(wishAdvertisementInformationRequest.getAdvertisementTag()));
        newInformationBuilder.connectorIntroduce(this.wishAdvertisementInformation.getConnectorIntroduce());
        newInformationBuilder.period(wishAdvertisementInformationRequest.getPeriod());
        newInformationBuilder.pay(wishAdvertisementInformationRequest.getPay());

        if(wishAdvertisementInformationRequest.getConnectorImageUrl() != null) {
            newInformationBuilder.connectorImageUrl(wishAdvertisementInformationRequest.getConnectorImageUrl());
        }
        else {
            newInformationBuilder.connectorImageUrl(this.wishAdvertisementInformation.getConnectorImageUrl());
        }

        this.wishAdvertisementInformation = newInformationBuilder.build();
    }

    private AdvertisementTag makeAdvertisementTag(AdvertisementTag advertisementTagRequest) {
        AdvertisementTag.AdvertisementTagBuilder advertisementTagBuilder = AdvertisementTag.builder();

        AdvertisementTag requestTag = advertisementTagRequest;
        AdvertisementTag beforeTag = this.wishAdvertisementInformation.getAdvertisementTag();

        if(requestTag.getAudienceClass().length() <= 0)
            advertisementTagBuilder.audienceClass(beforeTag.getAudienceClass());
        else
            advertisementTagBuilder.audienceClass(requestTag.getAudienceClass());


        if(requestTag.getBroadcastCharacter().length() <= 0)
            advertisementTagBuilder.broadcastCharacter(beforeTag.getBroadcastCharacter());
        else
            advertisementTagBuilder.broadcastCharacter(requestTag.getBroadcastCharacter());

        if(requestTag.getRelatedField().length()  <= 0)
            advertisementTagBuilder.relatedField(beforeTag.getRelatedField());
        else
            advertisementTagBuilder.relatedField(requestTag.getRelatedField());

        return advertisementTagBuilder.build();
    }
}
