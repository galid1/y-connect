package com.galid.yconnect.domains.connector.controller;

import com.galid.yconnect.domains.connector.service.RegisterConnectorRequest;
import com.galid.yconnect.domains.connector.service.RegisterConnectorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class RegisterConnectorController {
    @Autowired
    private RegisterConnectorService registerConnectorService;

    @PostMapping("/connectors")
    @ResponseBody
    public ResponseEntity registerConnector(@RequestParam("connectorImageFile") MultipartFile connectorImageFile,
                                            @RequestParam("registerConnectorRequest")RegisterConnectorRequest registerConnectorRequest,
                                            Authentication authentication) {
        registerConnectorRequest.setConnectorImageFile(connectorImageFile);
        registerConnectorService.registerConnector(registerConnectorRequest, authentication.getName());

        return ResponseEntity.ok().build();
    }

}
