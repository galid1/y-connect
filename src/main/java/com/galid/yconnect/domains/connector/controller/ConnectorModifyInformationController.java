package com.galid.yconnect.domains.connector.controller;

import com.galid.yconnect.domains.connector.service.ConnectorModifyService;
import com.galid.yconnect.domains.connector.service.RegisterConnectorRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class ConnectorModifyInformationController {
    @Autowired
    private ConnectorModifyService modifyService;

    @PutMapping("/connectors")
    public ResponseEntity modifyConnectorInformation(@RequestParam(value = "connectorImageFile", required = false) MultipartFile connectorImageFile,
                                                     @RequestParam("registerConnectorRequest") RegisterConnectorRequest modifyConnectorRequest,
                                                     Authentication authentication) {
        modifyConnectorRequest.setConnectorImageFile(connectorImageFile);
        modifyService.modifyConnectorInformation(modifyConnectorRequest, authentication.getName());

        return ResponseEntity.ok().build();
    }
}
