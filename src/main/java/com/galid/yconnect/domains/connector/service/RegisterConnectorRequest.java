package com.galid.yconnect.domains.connector.service;

import com.galid.yconnect.domains.connector.domain.AdvertisementTag;
import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Embedded;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class RegisterConnectorRequest {
    @Setter
    private MultipartFile connectorImageFile;
    private String period;
    private int pay;
    private String connectorIntroduce;
    @Embedded
    private AdvertisementTag advertisementTag;
}
