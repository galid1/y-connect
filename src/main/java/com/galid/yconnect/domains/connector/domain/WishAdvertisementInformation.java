package com.galid.yconnect.domains.connector.domain;

import lombok.*;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;


@Embeddable
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class WishAdvertisementInformation {
    @Setter
    private String connectorImageUrl;
    private String period;
    private Integer pay;
    private String connectorIntroduce;
    @Embedded
    private AdvertisementTag advertisementTag;
}
