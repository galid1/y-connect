package com.galid.yconnect.domains.connector.service;

import com.galid.yconnect.domains.connector.domain.ConnectorEntity;
import com.galid.yconnect.domains.connector.domain.ConnectorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ConnectorService {
    @Autowired
    private ConnectorRepository connectorRepository;

    public ConnectorEntity getConnectorByUserId(long userId) {
        return connectorRepository.findByUserId(userId).orElse(null);
    }

    public List<ConnectorSummary> getConnectorSummaryList() {
        return connectorRepository.findAllByOrderByCreatedDateDesc()
                .stream()
                .map(connector -> toConnectorSummary(connector))
                .collect(Collectors.toList());
    }

    private ConnectorSummary toConnectorSummary(ConnectorEntity connectorEntity) {
        List<String> tagList = Arrays.asList(
                connectorEntity.getWishAdvertisementInformation().getAdvertisementTag().getAudienceClass(),
                connectorEntity.getWishAdvertisementInformation().getAdvertisementTag().getBroadcastCharacter(),
                connectorEntity.getWishAdvertisementInformation().getAdvertisementTag().getRelatedField()
        );

        return ConnectorSummary.builder()
                .connectorId(connectorEntity.getConnectorId())
                .connectorImageUrl(connectorEntity.getWishAdvertisementInformation().getConnectorImageUrl())
                .connectorIntroduce(connectorEntity.getWishAdvertisementInformation().getConnectorIntroduce())
                .tagList(tagList)
                .build();
    }
}
