package com.galid.yconnect.domains.connector.controller;

import com.galid.yconnect.domains.connector.service.AdvertisementTreatmentRequest;
import com.galid.yconnect.domains.connector.service.ConnectorAdvertisementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/connectors")
public class ConnectorAdvertisementController {

    @Autowired
    private ConnectorAdvertisementService connectorAdvertisementService;

    @PutMapping("/advertisements")
    public ResponseEntity acceptAdvertisement(@RequestBody AdvertisementTreatmentRequest request) {
        connectorAdvertisementService.treatAdvertisement(request);

        return ResponseEntity.ok().build();
    }

}
