package com.galid.yconnect.domains.connector.service;

import lombok.*;

import java.util.List;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ConnectorSummary {
    private long connectorId;
    private String connectorImageUrl;
    private String connectorIntroduce;
    private List<String> tagList;
}
