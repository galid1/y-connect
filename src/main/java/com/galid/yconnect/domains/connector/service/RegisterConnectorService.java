package com.galid.yconnect.domains.connector.service;

import com.galid.yconnect.common.util.S3FileUtil;
import com.galid.yconnect.domains.connector.domain.ConnectorEntity;
import com.galid.yconnect.domains.connector.domain.ConnectorRepository;
import com.galid.yconnect.domains.connector.domain.WishAdvertisementInformation;
import com.galid.yconnect.domains.user.domain.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Service
public class RegisterConnectorService {
    @Autowired
    private ConnectorRepository connectorRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private S3FileUtil fileUtil;

    @Transactional
    public void registerConnector(RegisterConnectorRequest request, String userEmail) {
        long userId = verifyExistConnectorRegister(userEmail);

        String connectorImageUrl = storeConnectorImageFile(request.getConnectorImageFile());

        connectorRepository.save(ConnectorEntity.builder()
                                    .wishAdvertisementInformation(toWishAdvertisementInformation(request, connectorImageUrl))
                                    .userId(userId)
                                    .build());
    }

    private WishAdvertisementInformation toWishAdvertisementInformation(RegisterConnectorRequest request, String connectorImageUrl) {
        return WishAdvertisementInformation.builder()
                .advertisementTag(request.getAdvertisementTag())
                .connectorImageUrl(connectorImageUrl)
                .pay(request.getPay())
                .period(request.getPeriod())
                .connectorIntroduce(request.getConnectorIntroduce())
                .build();
    }

    private String storeConnectorImageFile(MultipartFile file) {
        return fileUtil.uploadFile(file);
    }

    private long verifyExistConnectorRegister(String userEmail) {
        long userId = userRepository.findByEmail(userEmail).orElseThrow(() -> new IllegalArgumentException("존재하지 않는 유저 입니다."))
                .getUserId();

        if(connectorRepository.findByUserId(userId).isPresent()) {
            throw new IllegalArgumentException("이미 등록한 정보가 존재합니다.");
        }

        return userId;
    }
}
