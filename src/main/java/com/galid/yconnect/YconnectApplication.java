package com.galid.yconnect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class YconnectApplication {

	public static void main(String[] args) {
		SpringApplication.run(YconnectApplication.class, args);
	}

}
