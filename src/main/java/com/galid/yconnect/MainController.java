package com.galid.yconnect;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.galid.yconnect.domains.advertisement.domain.AdvertisementRepository;
import com.galid.yconnect.domains.connector.domain.ConnectorEntity;
import com.galid.yconnect.domains.connector.service.ConnectorService;
import com.galid.yconnect.domains.user.domain.UserEntity;
import com.galid.yconnect.domains.user.domain.UserRepository;
import com.galid.yconnect.domains.user.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.SessionAttribute;

import java.util.Arrays;

@Controller
public class MainController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ConnectorService connectorService;

    @Autowired
    private AdvertisementRepository advertisementRepository;

    @GetMapping("/main")
    public String getMain(@SessionAttribute("role") Role role, Authentication authentication, Model model) {
        UserEntity userEntity = userRepository.findByEmail(authentication.getName()).get();
        model.addAttribute("user", userEntity);

        if(role.isYoutuber()) {
            ConnectorEntity connector = connectorService.getConnectorByUserId(userEntity.getUserId());
            if(connector != null) {
                model.addAttribute("advertisementRequestList", advertisementRepository.findByConnectorIdOrderByCreatedDateDesc(connector.getConnectorId()));
                model.addAttribute("test", true);
                model.addAttribute("connector", connector);
            }
            return "main/youtuberMain";
        }
        else {
            model.addAttribute("advertisementRequestList", advertisementRepository.findAdvertisementRequestListByCorporationId(userEntity.getUserId()));
            model.addAttribute("corporation", userEntity);
            return "main/corporationMain";
        }
    }

    @GetMapping("/services")
    public String getServices() {
        return "services";
    }

    @GetMapping("/matching")
    public String getMatching() {
        return "matching";
    }

    @Autowired
    private ObjectMapper objMapper;

    @GetMapping("/connectorList")
    public String getUBridgeList(Model model) throws JsonProcessingException {
        model.addAttribute("connectorListData", objMapper.writeValueAsString(connectorService.getConnectorSummaryList()));
        model.addAttribute("connectorList", connectorService.getConnectorSummaryList());
        model.addAttribute("relatedField", Arrays.asList("스타트업", "IT", "제조업", "유통업", "농업"));
        model.addAttribute("audienceClass", Arrays.asList("청소년", "2030대", "4050대", "여성", "남성"));
        model.addAttribute("broadcastCharacter", Arrays.asList("리뷰방송", "현장방송", "토크방송", "다수출연", "1인방송"));
        return "connectorList";
    }

    @GetMapping("/faq")
    public String getFaq() {
        return "faq";
    }

    @GetMapping("/faq/counseling")
    public String getCounseling() {
        return "counseling";
    }

    @GetMapping("/usage")
    public String getUsage() {
        return "usage";
    }

}
