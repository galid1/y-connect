package com.galid.yconnect.common.util;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.stereotype.Component;

@Component
public class SimpleS3Client {
    public AmazonS3Client getS3Client(Regions regions) {
        return (AmazonS3Client) AmazonS3ClientBuilder
                .standard()
                .withRegion(regions)
                .build();
    }
}
