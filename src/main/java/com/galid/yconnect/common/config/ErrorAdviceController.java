package com.galid.yconnect.common.config;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ErrorAdviceController {
    @ExceptionHandler(value = IllegalArgumentException.class)
    public ResponseEntity handleException(Exception exception) {
        return ResponseEntity.badRequest().body(exception.getMessage());
    }
}
