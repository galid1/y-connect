package com.galid.yconnect.common.config.handlebars;

import com.galid.yconnect.domains.advertisement.domain.AdvertisementEntity;
import com.galid.yconnect.domains.advertisement.domain.AdvertisementRepository;
import com.galid.yconnect.domains.advertisement.domain.MatchState;
import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Options;
import org.springframework.stereotype.Component;
import pl.allegro.tech.boot.autoconfigure.handlebars.HandlebarsHelper;

import java.io.IOException;

@Component
@HandlebarsHelper
public class IsMatchedHelper {
    public Handlebars.SafeString isMatchedCorporation(AdvertisementRepository.AdvertisementRequestModel advertisementRequest) {
        if(advertisementRequest.getMatchState().equals(MatchState.MATCHING.getState()))
            return new Handlebars.SafeString(templateCancelableState(advertisementRequest.getAdvertisementId()));

        return new Handlebars.SafeString(templateUnCancelableState(advertisementRequest.getMatchState()));
    }

    private String templateCancelableState(long advertisementRequestId) {
        return
                "<div id='" + advertisementRequestId + "' class='cancelableStateContainer'>" +
                    "<span style='font-size:14px; color: white'> 요청 취소하기 </span>" +
                "</div>";
    }

    private String templateUnCancelableState(String state) {
        return
                "<div class='unCancelableStateContainer'>" +
                    "<span style='font-size:14px; color: white'>" + state + "</span>" +
                "</div>";
    }

    public Handlebars.SafeString isMatchedYoutuber(AdvertisementEntity advertisementEntity) throws IOException {
        if(advertisementEntity.getMatchState().equals(MatchState.MATCHING.getState())) {
            return new Handlebars.SafeString(templateNotYetProcessedState());
        }
        return new Handlebars.SafeString(templateAlreadyProcessedState());
    }

    private String templateNotYetProcessedState() {
        return "<div class='advertisementBtnContainer'> \n" +
                    "<div class='advertisementRejectBtn'> \n" +
                        "<span>거절</span> \n" +
                    "</div> \n" +
                    "<div class='advertisementAcceptBtn'> \n" +
                        "<span>광고 매칭 수락</span> \n" +
                    "</div> \n" +
                "</div>";
    }

    private String templateAlreadyProcessedState() {
        return "<div class='processedBtnContainer'> \n" +
                   "<span>이미 처리된 광고입니다.</span> \n" +
               "</div>";
    }
}
