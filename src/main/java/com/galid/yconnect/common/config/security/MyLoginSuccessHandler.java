package com.galid.yconnect.common.config.security;

import com.galid.yconnect.domains.user.model.Role;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class MyLoginSuccessHandler implements AuthenticationSuccessHandler {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        HttpSession session = request.getSession();

        String userRole = String.valueOf(authentication.getAuthorities().iterator().next());
        session.setAttribute("role", makeRole(userRole));
        response.sendRedirect("/main");
    }

    private Role makeRole(String userRole) {
        Role role = new Role(false, false);

        if(userRole.equals("YOUTUBER")) {
            role.onYoutuber();
        }
        else {
            role.onCorporation();
        }

        return role;
    }
}
