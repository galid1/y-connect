package com.galid.yconnect.common.config.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class MyExceptionControllerAdvice {
    @ExceptionHandler(value = IllegalArgumentException.class)
    public ResponseEntity handleException(Exception exception) {
        return ResponseEntity.badRequest()
                .body(exception.getMessage());
    }
}
