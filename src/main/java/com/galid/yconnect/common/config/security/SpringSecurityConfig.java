package com.galid.yconnect.common.config.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
//    https://www.baeldung.com/spring-security-track-logged-in-users 시큐리티 세션 사용자 추적

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .csrf()
            .disable()
            .authorizeRequests()
                .antMatchers("/css/**", "/images/**", "/js/**")
                .permitAll()
                .antMatchers("/", "/login", "/policy/**", "/users/signUp", "/users/signIn", "/users/logout", "/users/checking")
                .permitAll()
                .anyRequest()
                .authenticated()
            .and()
            .formLogin()
                .usernameParameter("username")
                .passwordParameter("password")
                .loginPage("/users/signIn")
                .loginProcessingUrl("/login")
                .successHandler(new MyLoginSuccessHandler())
                .failureHandler(new MyLoginFailureHandler())
            .and()
            .logout()
                .logoutUrl("/users/logout")
                .logoutSuccessUrl("/");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
